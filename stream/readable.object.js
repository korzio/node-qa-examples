const stream = require('stream')

const readable = (function(){
  const data = []
  const $ = new stream.Readable({
    objectMode: true,
    read() {
      // $.push(1)
    }
  })
  $.push({ a: 1 })

  return $
})()

readable.on('data', (data) => {
  console.log(data)
})

readable.on('end', () => {
  console.log('There will be no more data.');
});

process.nextTick(() => {
  console.log('finished')
  process.exit(0)
})