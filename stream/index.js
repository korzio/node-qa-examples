const stream = require('stream')

const writable = (function(){
  const data = []
  const $ = new stream.Writable({
    write(chunk, encoding, callback) {
      data.push(chunk.toString())
      callback()
    }
  })

  $.on('prefinish', () => {
    console.log(data)
  })

  return $
})()

writable.write('some data')
writable.write('some more data')
writable.end('done writing data')

writable.on('finish', () => {
  console.log('All writes are now complete.')
})

process.nextTick(() => {
  console.log('finished')
  process.exit(0)
})